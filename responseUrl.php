<?php

    ini_set("date.timezone", "Africa/Nairobi");
    require_once "vendor/autoload.php";
    require_once "MenuLogger.php";
    require_once "DB.php";
    require_once "Payment.php";

    // get post data
    $postdata = file_get_contents("php://input");

    $postdata = urldecode($postdata);

    // get order details
    $order_id = $_GET["3DSecureId"];
    $request_id = $_GET["request_id"];

    // save response in DB
    $data = array();
    $data["request_id"] = $request_id;
    $data["status_code"] = 200;
    $data["response"] = $postdata;

    $db = new DB();
    $response_id = $db->insert("response",$data,__FILE__.".".__LINE__." function ".__FUNCTION__);

    // process PARes
    $payment = new Payment("PROCESS_ACS_RESULT");
    $pares = $payment->getPares($postdata);
    $response = $payment->processAcsResultNVP($order_id, $pares, $response_id);

    $response_id = $response["response_id"];

    $response = $payment->nvpParts($response["response"]);

    $html = $response["3DSecure.authenticationRedirect.simple.htmlBodyContent"];

    $status = $response["3DSecure.summaryStatus"];

    $xid = $response["3DSecure.xid"];

    $threeDSecureId = $response["3DSecureId"];

    switch ($status) {

        case "AUTHENTICATION_ATTEMPTED":
            echo "Authentication was attempted but the card issuer did not perform the authentication \n";
            break;

        case "AUTHENTICATION_FAILED":
            echo "The cardholder failed the authentication \n BYE BYE";
            break;

        case "AUTHENTICATION_NOT_AVAILABLE":
            //echo "An internal error occurred and Authentication is not currently available. \n";
            $sql = "SELECT request.*,response.response FROM request INNER JOIN response ON request.id=response.request_id WHERE request.id = $request_id AND order_id = $threeDSecureId";

            $db = new DB();
            $data = $db->fetchFirst($sql);
            $transactionId = $data->transaction_id;
            $reference = $data->reference;

            $response = $payment->three3DPay($threeDSecureId, $transactionId, $reference, $status, $xid);

            echo print_r($response);

            break;

        case "AUTHENTICATION_SUCCESSFUL":

            $token = $response["3DSecure.authenticationToken"];
            $acsEci = $response["3DSecure.acsEci"];

            $sql = "SELECT request.*,response.response FROM request INNER JOIN response ON request.id=response.request_id WHERE request.id = $request_id AND order_id = $threeDSecureId";

            $db = new DB();
            $data = $db->fetchFirst($sql);
            $transactionId = $data->transaction_id;
            $reference = $data->reference;

            $response = $payment->three3DPay($threeDSecureId, $transactionId, $reference, $status, $xid,$token,$acsEci);

            echo print_r($response);
            break;

        default:
            echo "unknown response $status \n";
            break;
    }


?>