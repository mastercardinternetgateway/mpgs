<?php
    /**
     * Created by PhpStorm.
     * User: mukuha
     * Date: 1/23/18
     * Time: 11:26 AM
     */

    class Payment
    {
        private $config;
        private $transaction_type;
        private $path_to_ini_config_file = "configs.ini";
        private $logger;

        public function __construct($type)
        {
            $this->config = parse_ini_file($this->path_to_ini_config_file,true);
            $this->transaction_type = trim($type);
            $this->logger = new MenuLogger($this->config["log"]["directory"],$this->config["log"]["info"],$this->config["log"]["error"]);
        }

        /**
         * process a test REST JSON pay transaction
         *
         * @param $orderid
         * @param $transactionid
         */

        public function pay($orderid,$transactionid){

            // load configs from array
            $merchantId = $this->config["credentials"]["merchantId"];
            $username = $this->config["credentials"]["username"];
            $password = $this->config["credentials"]["password"];
            $userId = $this->config["credentials"]["userId"];

            $number = $this->config["testCard"]["number"];
            $expiry_month = $this->config["testCard"]["expiry_month"];
            $expiry_year = $this->config["testCard"]["expiry_year"];
            $securityCode = $this->config["testCard"]["securityCode"];

            $tag = "JSON REST ORDER#".$orderid." TX#".$transactionid;

            $url = $this->getURL();

            // generate URL
            $url .= "merchant/$merchantId/order/$orderid/transaction/$transactionid";

            $this->log($tag." URL ", $url);

            // build request parameters
            $apiOperation = $this->transaction_type;

            $order = array(
                "amount"=>10,
                "currency"=>"KES",
                "reference" => $this->getReference()
            );

            $transaction = array(
               "reference" => $this->getReference()
            );

            $threeD = array(
                "enrollmentStatus" => "NOT_ENROLLED",
            );

            $sourceOfFunds = array(
                "type" => "CARD",
                "provided" => array(
                    "card" => array(
                        "number" => $number,
                        "expiry" => array(
                            "month" => $expiry_month,
                            "year" => $expiry_year
                        ),
                        "securityCode" => $securityCode
                    )
                )
            );

            $array = array(
                "apiOperation" => $apiOperation,
                "order" => $order,
                "sourceOfFunds" => $sourceOfFunds,
                "transaction" => $transaction,
                "3DSecure" => $threeD
            );

            $request = json_encode($array);

            $this->log("$tag REQUEST", $request);


            // build curl

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);

            if (curl_error($curl))
                $response = "cURL Error: " . curl_errno($curl) . " - " . curl_error($curl);

            $this->log("$tag INFO", $info);
            $this->log("$tag RESPONSE", $response);
            curl_close($curl);

            echo $response ."\n\n";

        }

        /**
         * process a test NVP JSON pay transaction
         *
         * @param int $orderid
         * @param int $transactionid
         */
        public function payNVP($orderid,$transactionid){

            // load configs from array
            $merchantId = $this->config["credentials"]["merchantId"];
            $username = $this->config["credentials"]["username"];
            $password = $this->config["credentials"]["password"];
            $userId = $this->config["credentials"]["userId"];

            $number = $this->config["testCard"]["number"];
            $expiry_month = $this->config["testCard"]["expiry_month"];
            $expiry_year = $this->config["testCard"]["expiry_year"];
            $securityCode = $this->config["testCard"]["securityCode"];

            $tag = "NVP ORDER#".$orderid." TX#".$transactionid;

            $url = $this->getURL("nvp");

            $this->log($tag." URL ", $url);

            // build request parameters
            $apiOperation = $this->transaction_type;

            $order = array(
                "id" => $orderid,
                "amount"=>100,
                "currency"=>"KES",
                "reference" => $this->getReference()
            );

            $transaction = array(
                "id" => $transactionid,
                "reference" => $this->getReference()
            );

            $sourceOfFunds = array(
                "type" => "CARD",
                "provided" => array(
                    "card" => array(
                        "number" => $number,
                        "expiry" => array(
                            "month" => $expiry_month,
                            "year" => $expiry_year
                        ),
                        "securityCode" => $securityCode
                    )
                )
            );

            $threeD = array(
                "enrollmentStatus" => "NOT_ENROLLED"
            );

            $array = array(
                "apiOperation" => $apiOperation,
                "order" => $order,
                "sourceOfFunds" => $sourceOfFunds,
                "apiPassword" => $password,
                "apiUsername" => $username,
                "merchant" => $merchantId,
                "transaction" => $transaction,
                "3DSecure" => $threeD
            );

            $request = $this->toNVP($array);

            $request = ltrim($request,"&");
            $request = rtrim($request,"&");

            $this->log("$tag REQUEST", $request);


            // build curl

            $curl = curl_init($url);
            // set the content length HTTP header

            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded;charset=UTF-8"));

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);

            if (curl_error($curl))
                $response = "cURL Error: " . curl_errno($curl) . " - " . curl_error($curl);

            $this->log("$tag INFO", $info);
            $this->log("$tag RESPONSE", $response);
            curl_close($curl);

            echo $response."\n\n";

        }

        public function getURL($type = "JSON"){

            $version = $this->config["ma"]["version"];
            if($type == "JSON") {
                $gatewayUrl = $this->config["ma"]["gatewayUrl"];
            }
            else {
                $gatewayUrl = $this->config["ma"]["nvpURL"];
            }

            return "$gatewayUrl/version/$version/";

        }

        public function toNVP($array,$parent =""){

            $string = "";

            foreach ($array as $key=>$value) {

                if(is_array($value)){

                    $string .= $this->toNVP($value,$parent.".".$key);
                }
                else {

                    if($parent){
                        $key_value="$parent.$key";
                    }
                    else{
                        $key_value = $key;
                    }
                    $key_value = ltrim($key_value,".");
                    $key_value = rtrim($key_value,".");
                    $string .= "$key_value=".urlencode($value). "&";
                }

            }

            return $string;

        }

        /**
         * logs data to file
         *
         * @param string $tag
         * @param string $data
         */
        public function log($tag,$data){

            if(is_array($data) || is_object($data)){

                $data = print_r($data,1);

            }

            // hide card details from logging
            $regex = '/"number":"[^"]*?"/';
            $data = preg_replace($regex, '"number":********', $data);

            $regex = '/"month":"[^"]*?"/';
            $data = preg_replace($regex, '"month":**', $data);

            $regex = '/"year":"[^"]*?"/';
            $data = preg_replace($regex, '"year":**', $data);

            $regex = '/"securityCode":"[^"]*?"/';
            $data = preg_replace($regex, '"securityCode":***', $data);

            $this->logger->DEBUG(" $tag   $data");
        }

        public function getReference($random_string_length = 6){

            $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $string = '';
            $max = strlen($characters) - 1;
            for ($i = 0; $i < $random_string_length; $i++) {
                $string .= $characters[mt_rand(0, $max)];
            }

            return $string;
        }

        /**
         * perfom 3DS Enrollment
         *
         * @param $orderid
         *
         * @return array
         */

        public function threeDSEnrollment($orderid){

            // load configs from array
            $merchantId = $this->config["credentials"]["merchantId"];
            $username = $this->config["credentials"]["username"];
            $password = $this->config["credentials"]["password"];
            $userId = $this->config["credentials"]["userId"];

            $number = $this->config["testCard"]["number"];
            $expiry_month = $this->config["testCard"]["expiry_month"];
            $expiry_year = $this->config["testCard"]["expiry_year"];
            $securityCode = $this->config["testCard"]["securityCode"];

            $tag = "threeDSEnrollment 3DSecureId#".$orderid;

            $url = $this->getURL();

            // generate URL
            $url .= "merchant/$merchantId/3DSecureId/$orderid";

            $this->log($tag." URL ", $url);

            // build request parameters
            $apiOperation = "CHECK_3DS_ENROLLMENT";

            $order = array(
                "amount"=>10,
                "currency"=>"KES",
            );

            $reference = $this->getReference();

            $transaction = array(
                "reference" => $reference
            );

            $threeD = array(
                "authenticationRedirect" => array(
                    "responseUrl" => "http://35.189.221.84/mpgs/responseUrl.php?3DSecureId=$orderid&request_id="
                ),
                "goodsDescription" => "Purchased at SOUTHWELL "
            );

            $sourceOfFunds = array(
                "provided" => array(
                    "card" => array(
                        "number" => $number,
                        "expiry" => array(
                            "month" => $expiry_month,
                            "year" => $expiry_year
                        )
                    )
                )
            );

            $array = array(
                "apiOperation" => $apiOperation,
                "order" => $order,
                "sourceOfFunds" => $sourceOfFunds,
                "3DSecure" => $threeD
            );

            $request = json_encode($array);

            $data = array();

            $data["order_id"] = $orderid;
            $data["transaction_id"] = $orderid;
            $data["apiOperation"] = $apiOperation;
            $data["endpoint"] = $url;
            $data["reference"] = $reference;
            $data["request"] = $request;

            $db = new DB();

            $request_id = $db->insert("request",$data,__FILE__.".".__LINE__." function ".__FUNCTION__);

            $threeD = array(
                "authenticationRedirect" => array(
                    "responseUrl" => "http://35.189.221.84/mpgs/responseUrl.php?3DSecureId=$orderid&request_id=$request_id"
                ),
                "goodsDescription" => "Purchased at SOUTHWELL "
            );

            $array = array(
                "apiOperation" => $apiOperation,
                "order" => $order,
                "sourceOfFunds" => $sourceOfFunds,
                "3DSecure" => $threeD
            );

            $request = json_encode($array);

            $this->log("$tag REQUEST", $request);


            // build curl
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if (curl_error($curl))
                $response = "cURL Error: " . curl_errno($curl) . " - " . curl_error($curl);

            $this->log("$tag INFO", $info);
            $this->log("$tag RESPONSE", $response);
            curl_close($curl);

            $data = array();
            $data["request_id"] = $request_id;
            $data["status_code"] = $status_code;
            $data["response"] = $response;

            $db = new DB();

            $response_id = $db->insert("response",$data,__FILE__.".".__LINE__." function ".__FUNCTION__);

            return array("response"=>$response,"response_id"=>$request_id);
        }

        /**
         * process ACS RESULTS
         *
         * @param $orderid
         * @param $pares
         * @param $response_id
         *
         * @return array
         */

        public function processAcsResult($orderid,$pares,$response_id){

            // load configs from array
            $merchantId = $this->config["credentials"]["merchantId"];
            $username = $this->config["credentials"]["username"];
            $password = $this->config["credentials"]["password"];

            $tag = "PROCESS_ACS_RESULT 3DSecureId#".$orderid;

            $url = $this->getURL();

            // generate URL
            $url .= "merchant/$merchantId/3DSecureId/$orderid";

            $this->log($tag." URL ", $url);

            // build request parameters
            $apiOperation = $this->transaction_type;

            $threeD = array(
                "paRes" => $pares
            );

            $array = array(
                "apiOperation" => $apiOperation,
                "3DSecure" => $threeD
            );

            $request = json_encode($array);

            $data = array();

            $data["order_id"] = $orderid;
            $data["transaction_id"] = $response_id;
            $data["apiOperation"] = $apiOperation;
            $data["endpoint"] = $url;
            $data["request"] = $request;

            $db = new DB();
            $request_id = $db->insert("request",$data,__FILE__.".".__LINE__." function ".__FUNCTION__);
            $this->log("$tag REQUEST", $request);

            // build curl
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if (curl_error($curl))
                $response = "cURL Error: " . curl_errno($curl) . " - " . curl_error($curl);

            //$this->log("$tag INFO", $info);
            $this->log("$tag RESPONSE", $response);
            curl_close($curl);

            $data = array();
            $data["request_id"] = $request_id;
            $data["status_code"] = $status_code;
            $data["response"] = $response;

            $db = new DB();

            $response_id = $db->insert("response",$data,__FILE__.".".__LINE__." function ".__FUNCTION__);

            return array("response"=>$response,"response_id"=>$request_id);
        }

        /**
         * process ACS RESULTS
         *
         * @param $orderid
         * @param $pares
         * @param $response_id
         *
         * @return array
         */

        public function processAcsResultNVP($orderid,$pares,$response_id){

            // load configs from array
            $merchantId = $this->config["credentials"]["merchantId"];
            $username = $this->config["credentials"]["username"];
            $password = $this->config["credentials"]["password"];

            $tag = "PROCESS_ACS_RESULT 3DSecureId#".$orderid;

            $url = $this->getURL();

            // generate URL
            $url .= "merchant/$merchantId/3DSecureId/$orderid";
            $url = $this->getURL("nvp");

            $this->log($tag." URL ", $url);

            // build request parameters
            $apiOperation = $this->transaction_type;

            $threeD = array(
                "paRes" => $pares
            );

            $array = array(
                "apiOperation" => $apiOperation,
                "3DSecure" => $threeD,
                "apiPassword" => $password,
                "apiUsername" => $username,
                "merchant" => $merchantId,
                "3DSecureId" => $orderid,
            );

            $request = $this->toNVP($array);

            $data = array();

            $data["order_id"] = $orderid;
            $data["transaction_id"] = $response_id;
            $data["apiOperation"] = $apiOperation;
            $data["endpoint"] = $url;
            $data["request"] = $request;

            $db = new DB();
            $request_id = $db->insert("request",$data,__FILE__.".".__LINE__." function ".__FUNCTION__);
            $this->log("$tag REQUEST", $request);

            // build curl
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded;charset=UTF-8"));
            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if (curl_error($curl))
                $response = "cURL Error: " . curl_errno($curl) . " - " . curl_error($curl);

            //$this->log("$tag INFO", $info);
            $this->log("$tag RESPONSE", $response);
            curl_close($curl);

            $data = array();
            $data["request_id"] = $request_id;
            $data["status_code"] = $status_code;
            $data["response"] = $response;

            $db = new DB();

            $response_id = $db->insert("response",$data,__FILE__.".".__LINE__." function ".__FUNCTION__);

            return array("response"=>$response,"response_id"=>$request_id);
        }

        public function getResponse($response){

            $array = json_decode($response,1);

        }

        public function getPares($query){

            $params = array();

            foreach (explode('&', $query) as $chunk) {

                $param = explode("=", $chunk);

                if ($param) {
                    $params[$param[0]] = $param[1];
                }
            }

            return $params["PaRes"];
        }

        public function nvpParts($query){

            $params = array();

            foreach (explode('&', $query) as $chunk) {

                $param = explode("=", $chunk);

                if ($param) {
                    $params[$param[0]] = urldecode($param[1]);
                }
            }

            return $params;
        }

        public function three3DPay($three3DId,$transactionid,$reference,$authenticationStatus,$xid,$authenticationToken = false,
                                   $acsEci = false ){

            // load configs from array
            $merchantId = $this->config["credentials"]["merchantId"];
            $username = $this->config["credentials"]["username"];
            $password = $this->config["credentials"]["password"];
            $userId = $this->config["credentials"]["userId"];

            $number = $this->config["testCard"]["number"];
            $expiry_month = $this->config["testCard"]["expiry_month"];
            $expiry_year = $this->config["testCard"]["expiry_year"];
            $securityCode = $this->config["testCard"]["securityCode"];

            $tag = "JSON REST 3D ORDER#".$three3DId." TX#".$transactionid;

            $url = $this->getURL();

            // generate URL
            $url .= "merchant/$merchantId/order/$three3DId/transaction/$transactionid";

            $this->log($tag." URL ", $url);

            // build request parameters
            $apiOperation = "PAY";

            $order = array(
                "amount"=>100,
                "currency"=>"KES",
                "reference" => $reference
            );

            $transaction = array(
                "reference" => $reference
            );

            $threeD = array(
                "enrollmentStatus" => "ENROLLED",
                "authenticationStatus" => $authenticationStatus,
                "xid" => $xid
            );

            if($acsEci){

                $threeD["acsEci"] = $acsEci;
            }

            if($authenticationToken){

                $threeD["authenticationToken"] = $authenticationToken;
            }

            $sourceOfFunds = array(
                "type" => "CARD",
                "provided" => array(
                    "card" => array(
                        "number" => $number,
                        "expiry" => array(
                            "month" => $expiry_month,
                            "year" => $expiry_year
                        ),
                        "securityCode" => $securityCode
                    )
                )
            );

            $array = array(
                "apiOperation" => $apiOperation,
                "order" => $order,
                //"3DSecureId" => $three3DId,
                "sourceOfFunds" => $sourceOfFunds,
                "transaction" => $transaction,
                "3DSecure" => $threeD
            );

            $request = json_encode($array);


            $data = array();

            $data["order_id"] = $three3DId;
            $data["transaction_id"] = $transactionid;
            $data["apiOperation"] = $apiOperation;
            $data["endpoint"] = $url;
            $data["request"] = $request;

            $db = new DB();
            $request_id = $db->insert("request",$data,__FILE__.".".__LINE__." function ".__FUNCTION__);
            $this->log("$tag REQUEST", $request);

            $this->log("$tag REQUEST", $request);


            // build curl

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);

            if (curl_error($curl))
                $response = "cURL Error: " . curl_errno($curl) . " - " . curl_error($curl);

            $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $this->log("$tag RESPONSE", $response);
            curl_close($curl);

            $data = array();
            $data["request_id"] = $request_id;
            $data["status_code"] = $status_code;
            $data["response"] = $response;

            $db = new DB();

            $response_id = $db->insert("response",$data,__FILE__.".".__LINE__." function ".__FUNCTION__);

            return array("response"=>$response,"response_id"=>$response_id);

        }

        public function authorize($orderid,$transactionid){

            // load configs from array
            $merchantId = $this->config["credentials"]["merchantId"];
            $username = $this->config["credentials"]["username"];
            $password = $this->config["credentials"]["password"];
            $userId = $this->config["credentials"]["userId"];

            $number = $this->config["testCard"]["number"];
            $expiry_month = $this->config["testCard"]["expiry_month"];
            $expiry_year = $this->config["testCard"]["expiry_year"];
            $securityCode = $this->config["testCard"]["securityCode"];

            $tag = "JSON REST ORDER#".$orderid." TX#".$transactionid;

            $url = $this->getURL();

            // generate URL
            $url .= "merchant/$merchantId/order/$orderid/transaction/$transactionid";

            $this->log($tag." URL ", $url);

            // build request parameters
            $apiOperation = $this->transaction_type;

            $order = array(
                "amount"=>10,
                "currency"=>"KES",
                "reference" => $this->getReference()
            );

            $transaction = array(
                "reference" => $this->getReference()
            );

            $threeD = array(
                "enrollmentStatus" => "NOT_ENROLLED",
            );

            $sourceOfFunds = array(
                "type" => "CARD",
                "provided" => array(
                    "card" => array(
                        "number" => $number,
                        "expiry" => array(
                            "month" => $expiry_month,
                            "year" => $expiry_year
                        ),
                        "securityCode" => $securityCode
                    )
                )
            );

            $array = array(
                "apiOperation" => $apiOperation,
                "order" => $order,
                "sourceOfFunds" => $sourceOfFunds,
                "transaction" => $transaction,
                "3DSecure" => $threeD
            );

            $request = json_encode($array);

            $this->log("$tag REQUEST", $request);


            // build curl

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);

            if (curl_error($curl))
                $response = "cURL Error: " . curl_errno($curl) . " - " . curl_error($curl);

            $this->log("$tag INFO", $info);
            $this->log("$tag RESPONSE", $response);
            curl_close($curl);

            echo $response ."\n\n";

        }

        public function retrieve3DS($orderid,$pares,$response_id){

            // load configs from array
            $merchantId = $this->config["credentials"]["merchantId"];
            $username = $this->config["credentials"]["username"];
            $password = $this->config["credentials"]["password"];

            $tag = "PROCESS_ACS_RESULT 3DSecureId#".$orderid;

            $url = $this->getURL();

            // generate URL
            $url .= "merchant/$merchantId/3DSecureId/$orderid";

            $this->log($tag." URL ", $url);

            // build request parameters
            $apiOperation = "PROCESS_ACS_RESULT";

            $threeD = array(
                "paRes" => $pares
            );

            $array = array(
                "apiOperation" => $apiOperation,
                "3DSecure" => $threeD
            );

            $request = json_encode($array);

            $data = array();

            $data["order_id"] = $orderid;
            $data["transaction_id"] = $response_id;
            $data["apiOperation"] = $apiOperation;
            $data["endpoint"] = $url;
            $data["request"] = $request;

            $db = new DB();
            $request_id = $db->insert("request",$data,__FILE__.".".__LINE__." function ".__FUNCTION__);
            $this->log("$tag REQUEST", $request);


            // build curl
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
            //curl_setopt($curl, CURLOPT_HTTPHEADER, array( "Accept: application/json" ));
            //curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if (curl_error($curl))
                $response = "cURL Error: " . curl_errno($curl) . " - " . curl_error($curl);

            //$this->log("$tag INFO", $info);
            $this->log("$tag RESPONSE", $response);
            curl_close($curl);

            $data = array();
            $data["request_id"] = $request_id;
            $data["status_code"] = $status_code;
            $data["response"] = $response;

            $db = new DB();

            $response_id = $db->insert("response",$data,__FILE__.".".__LINE__." function ".__FUNCTION__);

            return array("response"=>$response,"response_id"=>$request_id);
        }

    }