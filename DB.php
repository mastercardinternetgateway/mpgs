<?php

/**
* this class defines database connections
*/

class DB
    {
        private $config;
        private $path_to_ini_config_file = "configs.ini";
        private $logger;
        private $conn;

    /**
     * DB constructor.
     *
     * @param $type
     */
        public function __construct()
            {
                $this->config = parse_ini_file($this->path_to_ini_config_file,true);
                $this->logger = new MenuLogger($this->config["log"]["directory"],$this->config["log"]["info"],$this->config["log"]["error"]);

                $this->conn = new PDO('mysql:host='.$this->config["db"]["dbhost"].';dbname='.$this->config["db"]["dbname"], $this->config["db"]["dbuser"], $this->config["db"]["dbpassword"]);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }



        /**
        * executes fetch queries
        */

        public function fetch($sql,$bindingParams = null,$line = null,$db = null)
            {
                try
                    {
                        $stmt = $this->conn->prepare($sql);
                        $stmt->execute($bindingParams);
                        $results = array();
                        while($row = $stmt->fetchObject())
                            {
                                $results[] = $row;
                            }

                        $stmt = null;   
                        $this->conn = null;
                        return $results;
                    }
                catch(Exception $ex)
                    {
                        $this->logger->EXCEPTION(" SQL $line  ".$ex->getMessage());
                        $stmt = null;   
                        $this->conn = null;
                        throw $ex;
                    }
            }

        /**
        /* executes fetch queries
        */

        public function fetchFirst($sql,$bindingParams = null,$line = null)
            {
                try
                {
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute($bindingParams);
                    $results = array();
                    while($row = $stmt->fetchObject())
                    {
                        $stmt = null;
                        $this->conn = null;
                        return $row;
                    }
                }
                catch(Exception $ex)
                {
                    $this->logger->EXCEPTION(" SQL $line  ".$ex->getMessage());
                    $stmt = null;
                    $this->conn = null;
                    throw $ex;
                }
            }

        public  function insert($table,$data = array(),$line = null)
            {
                $t1 = Self::getmicrotime();

                $keys = array_keys($data);

                $params = array();

                $fields = array();

                foreach ($data as $key=>$value){

                    $fields[] = $key;
                    $binding_fields[] = ":$key";
                    $params[":$key"] = $value;
                }

                $sql = "INSERT INTO $table (".implode(",",$keys).",created) VALUES (".implode(",", $binding_fields).",now())";

                try
                    {

                        $stmt = $this->conn->prepare($sql);
                        $stmt->execute($params);
                        $lastInsertId = $this->conn->lastInsertId();
                        $stmt = null;   
                        $conn = null;
                        return $lastInsertId;
                    }
                catch(Exception $ex)
                    {
                        $this->logger->EXCEPTION(" SQL $line  ".$ex->getMessage());
                        $stmt = null;   
                        $this->conn = null;
                        throw $ex;
                    }
            }   

        public function update($sql,$bindingParams = null,$line = null)
            {
                try
                    {
                        $stmt = $this->conn->prepare($sql);
                        $stmt->execute($bindingParams);
                        $rowCount = $stmt->rowCount();
                        $stmt = null;   
                        $this->conn = null;
                        return $rowCount;
                    }
                catch(Exception $ex)
                    {
                        $this->logger->EXCEPTION(" SQL $line ".$ex->getMessage());
                        $stmt = null;   
                        $this->conn = null;
                        throw $ex;
                    }
            }

        public function batch($sql,$load=array(),$line = null)
            {
                try
                    {
                        $this->conn->beginTransaction();
                        $stmt = $this->conn->prepare($sql);
                        $rowCount = 0;

                        foreach ($load as $key => $value) 
                            {
                                $status=$stmt->execute($value);
                                $rowCount = $rowCount + $status;
                            }

                        $this->conn->commit();
                        $stmt = null;   
                        $this->conn = null;
                        return $rowCount;
                    }
                catch(Exception $ex)
                    {
                        $this->logger->EXCEPTION(" SQL $line ".$ex->getMessage());
                        $stmt = null;   
                        $this->conn = null;
                        throw $ex;
                    }
            }

        public static function getmicrotime() {

            list ($msec, $sec) = explode(" ", microtime());
            return ((float) $msec + (float) $sec);
        }              
    }
?>
