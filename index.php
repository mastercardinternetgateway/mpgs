<?php
    ini_set("date.timezone", "Africa/Nairobi");
    require_once "vendor/autoload.php";
    require_once "MenuLogger.php";
    require_once "DB.php";
    require_once "Payment.php";

    $payment = new Payment("PAY");
    $response = $payment->threeDSEnrollment(date("His"));

    $response_id = $response["response_id"];

    $array = json_decode($response["response"],1);

    $status = false;

    if(isset($array["response"]["3DSecure"]["gatewayCode"])){

        $status = $array["response"]["3DSecure"]["gatewayCode"];
    }

    if(!$status) {

        echo "invalid response from gateway \n";
        return;
    }

    $xid = false;

    if(isset($array["3DSecure"]["summaryStatus"])){

        $status = $array["3DSecure"]["summaryStatus"];

        switch ($status) {

            case "AUTHENTICATION_ATTEMPTED":
                echo "Authentication was attempted but the card issuer did not perform the authentication \n";
                break;

            case "AUTHENTICATION_FAILED":
                echo "The cardholder failed the authentication \n";
                break;

            case "AUTHENTICATION_NOT_AVAILABLE":
                echo "An internal error occurred and Authentication is not currently available. \n";
                break;

            case "AUTHENTICATION_SUCCESSFUL":
                echo "The cardholder was successfully authenticated. \n";
                break;

            case "CARD_DOES_NOT_SUPPORT_3DS":
                echo " The card does not support 3DS authentication. \n";
                break;

            case "CARD_ENROLLED":
                echo "The card is enrolled for 3DS authentication. \n";
                $xid = $array["3DSecure"]["xid"];
                $html = $array["3DSecure"]["authenticationRedirect"]["simple"]["htmlBodyContent"];
                echo $html;

                break;

            case "CARD_NOT_ENROLLED":
                echo "The card is not enrolled for 3DS authentication. \n";
                break;

            default:
                echo "unknown response $status \n";
                break;
        }
    }

?>